execute pathogen#infect()

" NERDTree
map <C-E> :NERDTreeToggle<CR>

set shiftwidth=4
set expandtab
"set tabstop=4
set softtabstop=4
set smarttab

if filereadable(expand("~/.vim/bundle/vim-colors-solarized/colors/solarized.vim"))
    let g:solarized_termcolors=256
    let g:solarized_termtrans=1
    set background="dark"
    color solarized " Load a colorscheme
else
    colorscheme peachpuff
endif

" Tab
nnoremap <Space> :tabnext<CR>
