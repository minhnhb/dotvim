Installation::

    git clone https://minhnhb@bitbucket.org/minhnhb/dotvim.git ~/.vim

Create symlinks::

    ln -s ~/.vim/vimrc ~/.vimrc

Switch to the ``~/.vim`` directory, and run init script::

    cd ~/.vim
    ./init.sh

The above will checkout the submodule commits specified in the super-repo (i.e.
this repo). They might be outdated by the time you clone this repo. Moreover,
periodically you might want to update to the latest commit (on ``master``
branch). Execute the following commands::

    cd ~/.vim
    # only needed right after 'submodule update'
    git submodule foreach git checkout master
    git submodule foreach git pull